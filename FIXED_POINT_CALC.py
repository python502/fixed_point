"""
	Fixed Point Calculator
	By:		BRV
	Date:	16/07/2022
	References:
		https://en.wikipedia.org/wiki/Q_(number_format)
		https://wiki.python.org/moin/BitwiseOperators
		http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/5-repr/fixed.html
		https://docs.python.org/3/library/enum.html
		https://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python
		https://witscad.com/course/computer-architecture/chapter/fixed-point-arithmetic-division
	Notes:
		Generate exe
		pyinstaller --onefile FIXED_POINT_CALC.py
	TODO:
		Implement Division
		Implement accumulators for easier math opperations
		Test A LOT!
"""
import enum
import FixedP_ALU

class Menu(enum.Enum):
	EXITMENU	= enum.auto()
	HEX2NUM		= enum.auto()
	NUM2HEX		= enum.auto()
	TWOCOMPL	= enum.auto()
	OPER2VALS	= enum.auto()
	ADD2VALS	= enum.auto()
	SUB2VALS	= enum.auto()
	MUL2VALS	= enum.auto()
	DIV2VALS	= enum.auto()
	EXECDEMO	= enum.auto()
	EXAMPLES	= enum.auto()
	MENULENGHT	= enum.auto()

MenuList = ["NA"]*Menu.MENULENGHT.value
MenuList.insert(Menu.EXITMENU.value,"Exit")
MenuList.insert(Menu.HEX2NUM.value, "Translate Hex to Number")
MenuList.insert(Menu.NUM2HEX.value, "Translate Number to Hex")
MenuList.insert(Menu.TWOCOMPL.value,"2's Complement Operation")
MenuList.insert(Menu.OPER2VALS.value,"Operate 2 Values")
MenuList.insert(Menu.ADD2VALS.value,"Add operation in HEX")
MenuList.insert(Menu.SUB2VALS.value,"Sub operation in HEX")
MenuList.insert(Menu.MUL2VALS.value,"MUL operation in HEX")
MenuList.insert(Menu.DIV2VALS.value,"DIV operation in HEX")
MenuList.insert(Menu.EXECDEMO.value,"Demo")
MenuList.insert(Menu.EXAMPLES.value,"Examples")
#print(MenuList)
#print(MenuList[Menu.EXITMENU.value])

resp = -1

while resp >= -1 and resp < Menu.MENULENGHT.value:
	
	match resp:
		case	Menu.EXITMENU.value:
			exit()

		case	Menu.HEX2NUM.value:
			print("------------------------------------------------------")
			print("Format:")
			print("Value,Qm.n")
			command = input()
			command = FixedP_ALU.Reformat(command)
			print("Fixed Point Representation: "+str(FixedP_ALU.Translate_Hex2Num(command)))
			print("------------------------------------------------------")

		case	Menu.NUM2HEX.value:
			print("------------------------------------------------------")
			print("Format:")
			print("Value,Qm.n")
			command = input()
			command = FixedP_ALU.Translate_int2Hex(command)
			print(command)
			print([int(command[0],0), command[1]])
			print("------------------------------------------------------")

		case	Menu.TWOCOMPL.value:
			print("------------------------------------------------------")
			print("Format:")
			print("Value,Qm.n")
			demo_2C_1	= input()
			demo_2C_R	= FixedP_ALU.TwosComplement(demo_2C_1)
			print(demo_2C_R)
			print(FixedP_ALU.Display_Hex(demo_2C_R))
			print("------------------------------------------------------")

		case	Menu.OPER2VALS.value:
			print("------------------------------------------------------")
			print("Basic Math Operation")
			print("WIP")
			print("------------------------------------------------------")

		case	Menu.ADD2VALS.value:
			print("------------------------------------------------------")
			print("Add operation in HEX")
			print("Format:")
			print("Value,Qm.n+Value,Qm.n")
			command = input()
			[Add_1, Add_2] = command.split("+")
			Add_1 = FixedP_ALU.Reformat(Add_1)
			Add_2 = FixedP_ALU.Reformat(Add_2)
			Add_R	= FixedP_ALU.Addition(Add_1,Add_2)
			print(Add_R)
			Add_R	= FixedP_ALU.Translate_Hex2Num(Add_R)
			print(Add_R)
			print("------------------------------------------------------")

		case	Menu.SUB2VALS.value:
			print("------------------------------------------------------")
			print("Sub operation in HEX")
			print("Format:")
			print("Value,Qm.n-Value,Qm.n")
			command = input()
			[Sub_1, Sub_2] = command.split("-")
			Sub_1 = FixedP_ALU.Reformat(Sub_1)
			Sub_2 = FixedP_ALU.Reformat(Sub_2)
			Sub_R	= FixedP_ALU.Subtraction(Sub_1, Sub_2)
			print(Sub_R)
			Sub_R	= FixedP_ALU.Translate_Hex2Num(Sub_R)
			print(Sub_R)
			print("------------------------------------------------------")

		case	Menu.MUL2VALS.value:
			print("------------------------------------------------------")
			print("Mul operation in HEX")
			print("Format:")
			print("Value,Qm.n*Value,Qm.n")
			command = input()
			[Mul_1, Mul_2] = command.split("*")
			Mul_1 = FixedP_ALU.Reformat(Mul_1)
			Mul_2 = FixedP_ALU.Reformat(Mul_2)
			Mul_R	= FixedP_ALU.Addition(Mul_1,Mul_2)
			print(Mul_R)
			Mul_R	= FixedP_ALU.Translate_Hex2Num(Mul_R)
			print(Mul_R)
			print("------------------------------------------------------")

		case	Menu.DIV2VALS.value:
			print("------------------------------------------------------")
			print("DIV operation in HEX")
			print("WIP")
			print("------------------------------------------------------")

		case	Menu.EXECDEMO.value:
			print("Entering Demo Mode")
			print("------------------------------------------------------")
			print("Translate 0x1F1F,Q10.5 to Fixed Point Representation")
			demo_TRH = FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_TRH = FixedP_ALU.Translate_Hex2Num(demo_TRH)
			print(demo_TRH)

			print("------------------------------------------------------")
			print("Translate 3.141592653589793238,Q3.12 to Hex")
			demo_TRD = FixedP_ALU.Translate_int2Hex("3.141592653589793238,Q3.12")
			print(demo_TRD)
			print([int(demo_TRD[0],0), demo_TRD[1]])
			print("Translate 62.2421875,Q8.7 to Hex")
			demo_TRD = FixedP_ALU.Translate_int2Hex("62.2421875,Q8.7")
			print(demo_TRD)
			print([int(demo_TRD[0],0), demo_TRD[1]])

			print("------------------------------------------------------")
			print("2s complement of 0x1F1F,Q10.5")
			demo_2C_1	= FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_2C_R	= FixedP_ALU.TwosComplement(demo_2C_1)
			print(FixedP_ALU.Display_Hex(demo_2C_R))
			print(demo_2C_R)

			print("------------------------------------------------------")
			print("Equalize Q values 0x1F1F,Q10.5 0x1F1F,Q8.7")
			demo_EQ_1	= FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_EQ_1_R	= FixedP_ALU.Translate_Hex2Num(demo_EQ_1)
			print(demo_EQ_1_R)
			demo_EQ_2	= FixedP_ALU.Reformat("0x1F1F,Q8.7")
			demo_EQ_2_R	= FixedP_ALU.Translate_Hex2Num(demo_EQ_2)
			print(demo_EQ_2_R)
			demo_EQ_R	= FixedP_ALU.Equalize_Q(demo_EQ_1,demo_EQ_2)
			print(FixedP_ALU.Display_Hex(demo_EQ_R[0]))
			print(FixedP_ALU.Display_Hex(demo_EQ_R[1]))
			demo_EQ_2_R_1	= FixedP_ALU.Translate_Hex2Num(demo_EQ_R[0])
			print(demo_EQ_2_R_1)
			demo_EQ_2_R_2	= FixedP_ALU.Translate_Hex2Num(demo_EQ_R[1])
			print(demo_EQ_2_R_2)

			print("Equalize Q values 0x1F1F,Q8.7 0x1F1F,Q10.5")
			demo_EQ_R = FixedP_ALU.Equalize_Q(demo_EQ_2,demo_EQ_1)
			print(FixedP_ALU.Display_Hex(demo_EQ_R[0]))
			print(FixedP_ALU.Display_Hex(demo_EQ_R[1]))
			demo_EQ_2_R_1	= FixedP_ALU.Translate_Hex2Num(demo_EQ_R[0])
			print(demo_EQ_2_R_1)
			demo_EQ_2_R_2	= FixedP_ALU.Translate_Hex2Num(demo_EQ_R[1])
			print(demo_EQ_2_R_2)

			print("------------------------------------------------------")
			print("Addition")
			print("Adding 0x1F1F,Q10.5 0x1F1F,Q8.7")
			demo_Add_1	= FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_Add_2	= FixedP_ALU.Reformat("0x1F1F,Q8.7")
			print(FixedP_ALU.Display_Hex(demo_Add_1))
			print(FixedP_ALU.Translate_Hex2Num(demo_Add_1))
			print(FixedP_ALU.Display_Hex(demo_Add_2))
			print(FixedP_ALU.Translate_Hex2Num(demo_Add_2))
			demo_Add_R	= FixedP_ALU.Addition(demo_Add_1,demo_Add_2)
			print(FixedP_ALU.Display_Hex(demo_Add_R))
			demo_Add_R	= FixedP_ALU.Translate_Hex2Num(demo_Add_R)
			print(demo_Add_R)

			print("------------------------------------------------------")
			print("Substraction")
			print("Substracting 0x1F1F,Q10.5 0x1F1F,Q8.7")
			demo_Sub_1	= FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_Sub_2	= FixedP_ALU.Reformat("0x1F1F,Q8.7")
			print(FixedP_ALU.Translate_Hex2Num(demo_Sub_1))
			print(FixedP_ALU.Translate_Hex2Num(demo_Sub_2))
			demo_Sub_R	= FixedP_ALU.Subtraction(demo_Sub_1, demo_Sub_2)
			print(FixedP_ALU.Display_Hex(demo_Sub_R))
			print(FixedP_ALU.Translate_Hex2Num(demo_Sub_R))

			print("------------------------------------------------------")
			print("Multiplication")
			print("Multuply 0x1F1F,Q10.5 0x1F1F,Q8.7")
			demo_Mul_1	= FixedP_ALU.Reformat("0x1F1F,Q10.5")
			demo_Mul_2	= FixedP_ALU.Reformat("0x1F1F,Q8.7")
			demo_Mul_R	= FixedP_ALU.Multiplication(demo_Mul_1,demo_Mul_2)
			print(FixedP_ALU.Display_Hex(demo_Mul_R))
			demo_Mul_R	= FixedP_ALU.Translate_Hex2Num(demo_Mul_R)
			print(demo_Mul_R)

			print("------------------------------------------------------")
		case	Menu.EXAMPLES.value:
			print("------------------------------------------------------")
			print("Examples:")
			print("HEX to num 	: 0x1F1F,Q10.5")
			print("Num to HEX	: 62.2421875,Q10.5 to Hex")
			print("2's Compl 	: 0x1F1F,Q10.5")
			print("Addition		: 0x1F1F,Q10.5+0x1F1F,Q7.8")
			print("Substraction	: 0x1F1F,Q10.5-0x1F1F,Q7.8")
			print("Multiplication	: 0x1F1F,Q10.5*0x1F1F,Q7.8")
			print("Divicion		: 0x1F1F,Q10.5/0x1F1F,Q7.8")
			print("------------------------------------------------------")

		case	_:
			print("Invalid response")
			resp = -1

	print("Welcome to Fixed Point Calculator v0.10!")
	print("What would you like to do?")
	print("Type a number")
	#	Print Menu
	for i in range(1,Menu.MENULENGHT.value):
		print(str(i)+" : "+ MenuList[i])
	resp = int(input())
