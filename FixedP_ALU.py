
from ast import If, Num
import enum
from multiprocessing.sharedctypes import Value
from tokenize import Number

from scipy.fftpack import shift


#	TODO : Change format from this [Value in HEX, [Qm , Qn, byte size]] to [Value in HEX, Qm , Qn, byte size]
#	It's more convinient with enums

class Q_f(enum.Enum):
	m	= 0
	n	= enum.auto()
	size= enum.auto()
	
class NumProp(enum.Enum):
	INTEGER	= 0
	Q_format= enum.auto()

#	Description:
#  		Changes a String Q format into an array lenght 3 that contains Q parameters
#	Parameter: str
#	Example:
#		"Q10.5"
#	Result:
#		[Qm , Qn, byte size]
def Interpretate_Q(data):
	if(data[0] != 'Q'):
		print("You forgot to add Q")
		return -1
	data = data[1:]	#Remove first char
	data = data.split(".")
	if(len(data) != 2):
		print("Error in Comma location or ammount")
		return -1
	if( isinstance(int(data[0], 0), int) != True):
		print("Unexpected value, m")
		return -1
	if( isinstance(int(data[1], 0), int) != True):
		print("Unexpected value, n")
		return -1
		
	data[0] = int(data[0])
	data[1] = int(data[1])
	Q = 1 + data[0] + data[1]

	if((Q == 8) or (Q == 16) or (Q == 32) or (Q == 64) or (Q == 128)):
		return [data[0], data[1], Q]
	
	print("Unexpected value, of Q: " + str(Q))
	print("Expected int8, 16, 32, 64, or 128")
	return -1

# TODO : Finish Translate_int2Hex Implementation
def Translate_int2Hex(Value):
	Value = Value.split(",")
	Res = [float(Value[0]), Interpretate_Q(Value[1])]
	decimal	= int(Res[NumProp.INTEGER.value])	#Get integer number
	decimal = ShiftLeft(decimal, Res[NumProp.Q_format.value][Q_f.n.value])	#Adjust Int to Q format
	fraction_r = 0
	fraction = Res[NumProp.INTEGER.value]
	for i in range(0,Res[NumProp.Q_format.value][Q_f.n.value]):
		fraction = 2* (fraction % 1)	#Get fraction
		binary = int(fraction)
		fraction_r = ShiftLeft(fraction_r,1) + binary
	return	[hex(decimal + fraction_r) , Res[NumProp.Q_format.value]]

#	Description:
# 		Changes a string Hexadecimal value into an Integer 
#	Parameter: Str
#		Example: "0x1F1F"
#	Result: int
def StringHex2Int(Hex):
	if( isinstance(int(Hex, 0), int) != True):
		print("Unexpected first value, may not be in HEX format")
		return -1
	return int(Hex, 0)

#	Description:
# 		Converts Hex with Q format value into a decimal value
#	Parameter:
#		[Value in HEX, [Qm , Qn, byte size]]
#	Example:
# 		 
#	Result: int
def Translate_Hex2Num(command):
	Q_format_R	= command
	Result = 0
	for i in range(0,Q_format_R[NumProp.Q_format.value][Q_f.size.value]):
		Result = Result + (1 & ShiftRight(Q_format_R[NumProp.INTEGER.value],i))*pow(2,i-Q_format_R[NumProp.Q_format.value][Q_f.n.value])
	return Result

#	Description:
# 		Converts string into a list with the following format [Value in HEX, [Qm , Qn, byte size]]
#	Parameter: Str
#		Example: "0x1F1F,Q10.5"
#	Result: 
# 		[Value in HEX, [Qm , Qn, byte size]]
def Reformat(command):
	command = command.split(",")
	if(len(command) != 2):
		print("Error in Comma location or ammount")
		return -1
	return  [StringHex2Int(command[NumProp.INTEGER.value]), Interpretate_Q(command[NumProp.Q_format.value])]

#	Description:
# 		Shifts data right n times 
#	Parameters:
# 		data (int), n (int) 
#	Result:
# 		int 
def ShiftRight(data, n):
    return data >> n

#	Description:
# 		Shifts data Left n times 
#	Parameters:
# 		data (int), n (int) 
#	Result:
# 		int 
def ShiftLeft(data, n):
    return data << n

#	Description:
# 		2's complement operation dependent on the Q size
# 	Paramerter: val
#  		Array of [Value in HEX, [Qm , Qn, int size]]
#   	Example: 
# 			[0x1F1F , [10 , 5, 16]]
#  Return: 
# 		[Value in HEX, [Qm , Qn, int size]]
def TwosComplement(val):
	val[NumProp.INTEGER.value] = (~val[NumProp.INTEGER.value] & (pow(2, val[NumProp.Q_format.value][Q_f.size.value]) -1 )) +1
	return val

#	Description:
# 		Makes two values with different Q formats equal Q formats
# 	Note:
# 		The value with the highest Qn has preference  
# 	Paramerters: Val1, Val2
#  		Arrays of [Value in HEX, [Qm , Qn, int size]]
#   	Example: 
# 			[0x1F1F , [10 , 5, 16]]
#  Return: 
# 		[[Value in HEX, [Qm , Qn, int size]] , [Value in HEX, [Qm , Qn, int size]]]
def Equalize_Q(Val1, Val2):
	
	adjustment = Val1[NumProp.Q_format.value][Q_f.n.value] - Val2[NumProp.Q_format.value][Q_f.n.value]
	if (adjustment != 0):	#Does the Q needs adjustment?
		if (adjustment < 0):	#Value 1 Needs Adjustement
			Val1[NumProp.INTEGER.value]	= ShiftLeft(Val1[NumProp.INTEGER.value],abs(adjustment))
			Val1[NumProp.Q_format.value][Q_f.n.value]	= Val2[NumProp.Q_format.value][Q_f.n.value]
		if (adjustment > 0):	#Value 2 Needs Adjustement
			Val2[NumProp.INTEGER.value]	= ShiftLeft(Val2[NumProp.INTEGER.value],abs(adjustment))
			Val2[NumProp.Q_format.value][Q_f.n.value]	= Val1[NumProp.Q_format.value][Q_f.n.value]

	if (Val1[NumProp.Q_format.value][Q_f.size.value] != Val2[NumProp.Q_format.value][Q_f.size.value]):	#Int size unmatch
		if (Val1[NumProp.Q_format.value][Q_f.size.value] > Val2[NumProp.Q_format.value][Q_f.size.value]):	#Int from Value 1 is bigger
			Val2[NumProp.Q_format.value][Q_f.size.value]	= 2*Val2[NumProp.Q_format.value][Q_f.size.value]
		if (Val1[NumProp.Q_format.value][Q_f.size.value] < Val2[NumProp.Q_format.value][Q_f.size.value]):	#Int from Value 2 is bigger
			Val1[NumProp.Q_format.value][Q_f.size.value]	= 2*Val1[NumProp.Q_format.value][Q_f.size.value]
	
	Val1[NumProp.Q_format.value][Q_f.m.value]	= Val1[NumProp.Q_format.value][Q_f.size.value] - Val1[NumProp.Q_format.value][Q_f.n.value]
	Val2[NumProp.Q_format.value][Q_f.m.value]	= Val2[NumProp.Q_format.value][Q_f.size.value] - Val2[NumProp.Q_format.value][Q_f.n.value]
	return  [Val1, Val2]


#	Description:
# 		Add 2 values with different or equal Q formats 
# 	Paramerters: Val1, Val2
#  		Arrays of [Value in HEX, [Qm , Qn, int size]]
#	Return:
# 		 [Value in HEX, [Qm , Qn, int size]]
def Addition(Val1, Val2):
	[Val1, Val2] = Equalize_Q(Val1, Val2)
	return  [Val1[NumProp.INTEGER.value]+Val2[NumProp.INTEGER.value],Val1[NumProp.Q_format.value]]

#	TODO : Finish Subtraction
#	Dependencies : 
def Subtraction(Val1, Val2):
	[Val1, Val2] = Equalize_Q(Val1, Val2)
	TwosC = TwosComplement(Val2)
	Result = (Val1[NumProp.INTEGER.value] + TwosC[NumProp.INTEGER.value]) & (pow(2, Val1[NumProp.Q_format.value][Q_f.size.value]) -1 )
	return  [Result, Val1[NumProp.Q_format.value]]

#	Description:
# 		Mult 2 values with different or equal Q formats 
# 	Paramerters: Val1, Val2
#  		Arrays of [Value in HEX, [Qm , Qn, int size]]
#	Return:
# 		 [Value in HEX, [Qm , Qn, int size]]
def Multiplication(Val1, Val2):
	Biggest_size	= 0
	if (Val1[NumProp.Q_format.value][Q_f.size.value] != Val2[NumProp.Q_format.value][Q_f.size.value]):
		if (Val1[NumProp.Q_format.value][Q_f.size.value] > Val2[NumProp.Q_format.value][Q_f.size.value]):
			Biggest_size = Val1[NumProp.Q_format.value][Q_f.size.value]
		if (Val1[NumProp.Q_format.value][Q_f.size.value] < Val2[NumProp.Q_format.value][Q_f.size.value]):
			Biggest_size = Val2[NumProp.Q_format.value][Q_f.size.value]
	else:
		Biggest_size = Val1[NumProp.Q_format.value][Q_f.size.value]
	Total = 0
	for i in range(0,Biggest_size):
		if (0x01 & ShiftRight(Val1[NumProp.INTEGER.value], i)):
			Total = Total + ShiftLeft(Val2[NumProp.INTEGER.value], i)
	return  [Total , [2*Biggest_size - (Val1[NumProp.Q_format.value][Q_f.n.value] + Val2[NumProp.Q_format.value][Q_f.n.value]), Val1[NumProp.Q_format.value][Q_f.n.value]+Val2[NumProp.Q_format.value][Q_f.n.value], 2*Biggest_size]]

#	TODO: Finish Division Implementation
#	Dependencies: Subtraction
def Division(Val1, Val2):
	return  1

#	Description:
# 		Translates decimal value to hex format (str)
# 	Paramerters: Val
#  		Arrays of [Value in HEX, [Qm , Qn, int size]]
#	Return:
# 		 [string HEX, [Qm , Qn, int size]]
def	Display_Hex(val):
	return [hex(val[NumProp.INTEGER.value]), val[NumProp.Q_format.value]]
