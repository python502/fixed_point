# FIXED_POINT_CALCULATOR

## Getting started

The only requirement for this tool is **Python 3.10** and **PyInstaller**. Use the following command on windows to install **PyInstaller**. For more information about this tool follow [this link](https://datatofish.com/executable-pyinstaller/)

    pip install pyinstaller

## Generate executable in windows
Once you have installed correctly **PyInstaller** type this command in the project folder. This will generate an executable file for the tool.

    pyinstaller --onefile FIXED_POINT_CALC.py

## References
-   [What's a Q format?](https://en.wikipedia.org/wiki/Q_(number_format))
-   [How to use Bitwise Operatos in Python](https://wiki.python.org/moin/BitwiseOperators)
-   [How to use enumerators in Python](https://docs.python.org/3/library/enum.html)
    http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/5-repr/fixed.html
-   [How to use Switch statement in Python](https://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python)
-   [Where do can I find more documentation about Pyinstaller?](https://datatofish.com/executable-pyinstaller/)

## Notes:

I'm open to suggestions on how to improve this tool. Please let me know!

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/python502/fixed_point/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***