
PROYECT=FIXED_POINT_CALC.py
ICON=Fixed_p.bmp


install:
	pip install pyinstaller
	pip install --upgrade pip
	pip install Pillow

run:
	pyinstaller --onefile ${PROYECT} \
	--icon=${ICON}
	@echo Build Finished!

help:
	@echo build: Creates .exe
	@echo install: Install all dependencies

